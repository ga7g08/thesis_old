#!/bin/bash

# Copy the pdf of Thesis git repo into the shared dropbox with DIJ

DESTINATION=/home/greg/Dropbox/Timing_Noise_Greg_Ashton/Thesis
PDF_DEPTH=2 # To copy the source images set as 3

for i in $(find -maxdepth $PDF_DEPTH -name "*.pdf"); do
    echo Copy: $i
    cp --parents $i $DESTINATION
done
