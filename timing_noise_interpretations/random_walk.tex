Timing noise was first quantified and interpreted by \citet{Boynton1972} as a
Poisson like random walk in one of the phase, frequency, or spindown. The
pulsar spins down according to the power law spindown of equation \eqref{eqn:
power law spindown} except that at random times the pulse phase, frequency or
spindown jumps, or changes discontinuously. The waiting times between events
are Poisson distributed with a rate R such that over a period $T$ the number of
events follows a Poisson distribution with mean RT. All the jumps are
independent and the magnitudes are randomly distributed with means given by
$\langle\dP\rangle$, $\langle\dF\rangle$, and $\langle\dS\rangle$ for the phase
frequency and spindown. We can investigate the statistical properties of such
random walks by splitting the phase into contributions from the secular
spindown $\phi_{\mathrm{S}}$ (as given by equation \eqref{eqn: Taylor compact})
and contributions from the random walks
\begin{equation}
    \phi = \phi_{\mathrm{S}} + \Delta\phi_{\mathrm{R}}
\end{equation}
The three random walks can be written as the sum of $N$ individual events
occurring at times $t_{i}$
\begin{align}
    \Delta\phi_{\mathrm{R}} & = \s{i=1}{N}\Delta\phi_{i} H(t - t_{i}) 
     && \mathrm{(phase),} \\
    \Delta\phi_{\mathrm{R}} & = \s{i=1}{N}\Delta\f_{i} (t-t_{i})H(t - t_{i}) 
     && \mathrm{(frequency),} \\
    \Delta\phi_{\mathrm{R}} & = \s{i=1}{N}\frac{1}{2}\Delta\fdot_{i} (t-t_{i})^{2}H(t - t_{i}) 
     && \mathrm{(spindown),}
\end{align}
where $H(t)$ is unit step function at $t=0$. It should be noted that here we are
treating the three types of noise separately such that timing noise residuals 
from either a random walk in phase, frequency, or spindown; work by \citet{Cordes1980}
extended this model to handle mixing between the types of noise.

Timing noise is the remainder having
fitted and subtracted a second order Taylor expansion. Provided the perturbations
of $\phi_{\mathrm{s}}$ are small then the remainder will be exactly given by 
$\phi_{\mathrm{s}}$. Then, as described by \citet{Boynton1972} we can then 
average over: the $\dP_{i}, \dF_{i}$ or $\dS_{i}$
distributions, the $t_{i}$ distribution, and the $N$ distributions to give
\begin{align}
    \langle \Delta\phi_{R} \rangle & = \langle \dP \rangle R T 
    = S_{\mathrm{PN}}T && \mathrm{(phase),} \\
    \langle \Delta\phi_{R} \rangle & = \frac{1}{2}\langle \dF \rangle R T^{2} 
    = \frac{1}{2}S_{\mathrm{FN}}T^{2} && \mathrm{(frequency),} \\
    \langle \Delta\phi_{R} \rangle & = \frac{1}{6}\langle \dS \rangle R T^{3} 
    = \frac{1}{6}S_{\mathrm{SN}}T^{3} && \mathrm{(spindown).} \\
\end{align}
Here we have implicitly defined the strength parameters which combine the rate 
and averaged magnitude of jumps into a single quantity. Measurements of timing 
noise, if they are discreet events, will observe the accumulation of many 
individual events. As a result only the strength can be
measured, not the rate and average magnitude.

The three types of noise are distinguishable by their dependence on the
observation time $T$. The type of noise can be measured by
translating this into the dispersion measure of
$\Delta\fddot$ after fitting a cubic, or by inspection of the power spectrum.
\citet{Boynton1972} was able to categorise the Crab pulsar as frequency like noise.
They found that over a $5$~year period the noise process was stationary
and consistent with the frequency noise hypothesis. No deterministic process
could account for the timing residuals strengthening their conviction that some
random process was taking place.  However, they suggested that over longer periods
the random walk will be non-stationary due to either mixing with other types of
walks, or decay of the strength parameter with time.

The interpretation of timing noise as a Poisson random walk is a purely
statistical model. It is however backed up for a rich variety of physical
models.  A key feature of any physical
random walk model is that it must be able to produce both increases and
decreases in the relevant parameter. For this reason it is felt unlikely that
the timing noise mechanism is the same as the glitch mechanism, although they
must be related.  In addition it is unclear if timing noise is a continuous or
discreet process, certainly if it is discreet the waiting time between events
must be shorter than the shortest observation periods $\sim days$. 

The first physical model was proposed by \citet{Boynton1972}, the noise process
consisted of the as accretion of small lumps of matter onto the NS from the
interstellar medium. Lumps of matter fall randomly onto the surface of the star
causing either a spin-up or slowdown through the transfer of angular momentum.
After this many models were proposed such as starquakes and the random pinning
and unpinning of vortex lines; these were reviewed by \citet{Cordes1981} and
evaluated against observational constraints. Of these only three mechanisms
where found to be consistent with observations: crust breaking by vortex pinning, a
response to heat pulses, and luminosity related torque fluctuations. Since this
review, new random walk mechanisms have been proposed such as: variations in
the magnetospheric gap size \citep{Cheng1987}; the interference by debris entering
the magnetosphere \citep{Cordes2008}; and the accumulation of multiple micro-glitches
\citep{Janssen2006}. It would be a useful exercise to review both the new and 
old mechanisms against the current observational catalogue.

The first measurement of individual events was made by \citet{Cordes1985} who
identified $\sim20$ events in both frequency and spindown which could not be
explained by a glitch. In the same work, considering 24 pulsars over a period
of~$\sim13$~years, the authors concluded that: the timing noise seen in the
data could not be explained solely by an idealised random walk processes in the
phase, or its derivatives. They suggested that most of the activity is due to a
mixture of events in the phase, frequency and/or frequency derivative.

A recent review, and the most comprehensive by far was performed by
\citet{Hobbs2010} for 366 pulsars. They found that, timing residuals tend to
admit quasi-periodic features when observed on sufficiently long time scales
$\gtrsim 2$~years. As such, the method of measuring the type and strength of
timing noise depends on the length and epoch of observation. This suggests a
pure random walk hypothesis is not entirely consistent with observations.
Nevertheless, it is unclear what repercussions the conclusions of
\citet{Hobbs2010} has for the physical origin of timing noise.

