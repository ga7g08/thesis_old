Recently a new model has been proposed by \citet{Lyne2010} to explain the
observation that, over long time periods, the timing noise structure is
quasi-periodic. This began with the observation by \citet{Kramer2006} that the
pulses from PSR B1931+24 where intermittent. The pulsar acts as a normal pulsar
for $\sim10$~days and then switches off, being undetectable for $\sim25$~days,
and then switching on again. Analysing the spindown rate between the on and off
states, they determined the spindown rate $\dot{\f}$ was $\sim50\%$ faster in
the on state. This figure illustrating this is reproduced in figure \ref{fig:
kramer 2006 fig2}.
\begin{figure}
    \centering
    \includegraphics[width=.5\textwidth]{Kramer_2006_fig2}
    \caption{Figure taken from \citet{Kramer2006} showing the switched spindown
             of pulsar PSR B1931+24}
    \label{fig: kramer 2006 fig2}
\end{figure}
In the  upper panel (\textbf{A}) the authors show the evolution of the
rotational frequency over a 160 day period encompassing several switching
events. The line shows the long-term spindown of the pulsar while the dots show
individual measurements made during the on state. During these on states the
gradient of the reduction in frequency is increased, that is the spindown has
increased. It is thought that measurements of the frequency in the off state
would produce a line with decreased spindown connecting the dots. This is
accompanied by the timing residual measured over the same period in the lower
panel (\textbf{B}). This shows significant quasi-periodic modulations in sync
with the switching. This work proposed that the switching was a magnetospheric
phenomenon. The sharpness of the switches certainly requires something acting
on a short timescale and it intuitively makes sense that the greater spindown
results from greater torque produced by the emissions observed during the on
state.

The authors of \citet{Lyne2010} then tested a range of other pulsars and
presented a study of 17 pulsars for which they claim evidence for two-state
switching. Unlike B1931+24 these pulsars are not intermittent but 
continuously pulse. Measuring the spindown as a function of time over a
$\sim20$~year period they demonstrated fluctuations as reproduced in figure
\ref{fig: lyne 2010 fig2}

\begin{figure}
    \centering
    \includegraphics[width=.5\textwidth]{Lyne_2010_fig2}
    \caption{Figure taken from \citet{Lyne2010} showing the spindown rate
             of 17 pulsars over a $\sim20$~year period.}
    \label{fig: lyne 2010 fig2}
\end{figure}

This plot shows smooth variations in the spindown with some pulsars better
behaved than others. The method used to calculate the spindown required
averaging over a $\sim100$~day period; the authors argue that, if the spindown
undergoes a sharp switch between two values, this will be smoothed out by the
averaging.  Therefore it is argued in \citet{Lyne2010} that figure \ref{fig:
lyne 2010 fig2} show the $\dot{nu}$ moving between a few (typically 2) well
defined values.  The authors noted a gradual long-term change in the spindown
for all pulsars across the data set, indicating a non-zero second order
spindown. 

In order to quantify the claim of \citet{Kramer2006} that the switching was
magnetospheric (e.g. the results of enhanced particle flow), \citet{Lyne2010}
looked for correlations between the pulse width, measuring the amount of
emission, with the spindown rate.  They found that for 6 pulsars the pulse
width was indeed wider during the higher spindown. However, these variations
are also smooth and subject to the same averaging process. To improve the
resolution they then show the individual measurements of pulse width for two of
the pulsars; these appear to demonstrate the switching happening
instantaneously between the two values. The authors argue this confirms that
the two-state switching is magnetospheric since this is the only mechanism able
to act on such short timescales. Intuitively it makes sense that changes in
the pulse shape, will be correlated with changed in the amount of emission and 
hence the spindown rate. 

It has not been established how these magnetospheric will manifest themselves.
In particular this interpretation lacks an explanation of how the magnetosphere is
regulated to stay in a stable state for long periods ($1-10$~years) but then
switch over $\lesssim100$~days.

\citet{Lyne2010} proposes that the quasi-periodic structure observed in many
timing residuals (e.g. see \citet{Hobbs2010}) could be explain by this
switching process. To quantify this, in the supplementary material they created
a simple model for the effect of switching in the value of the spindown
$\dot{\nu}$ on timing residuals. We will now repeat this experiment to outline
the results. 

Firstly we model a pulsar as spinning down in the usual way except that it's
spindown has two distinct values $\dot{\nu}_{A}$ and $\dot{\nu}_{B}$. Then we
define the model the ratio of time spent state in state $A$ and $B$ as $R =
t_{B}/t_{A}$.  This is a purely deterministic model and having generated the
spindown values we can integrated twice to get the phase. Fitting and
subtracting a quadratic polynomial leaves the phase residual. In figure
\ref{fig: lyne example D=0} we show a typical result; in the top figure is the
spindown values which we define and in the bottom the resulting structure in
the phase residual.
\begin{figure}[htb]
    \centering
    \includegraphics[width=.5\textwidth]{{R_3.0_D_0}.pdf}
    \caption{A deterministic realisation of the Lyne switched spindown model. The
             resulting structure in the timing residuals are strictly periodic.}
    \label{fig: lyne example D=0}
\end{figure}
The phase residuals in figure \ref{fig: lyne example D=0} are strictly
periodic. \citet{Lyne2010} realised that in order to fit the observed
quasi-periodic residuals a random element must be introduced. This can be done
by the form of a 'dither' $D$ in the waiting time between switches. Now we have
periods $t_{A}^{i}$ and $t_{B}^{i}$ which are Gaussian distributed with a mean
of $t_{A}$ and $t_{B}$ and a standard deviation $D t_{A}$ and $D t_{B}$. The
result is illustrated in figure \ref{fig: lyne example D=0.3}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.5\textwidth]{{R_3.0_D_0.3}.pdf}
    \caption{A realisation of the Lyne model with a random element producing the
             observed quasi-period structure.}
    \label{fig: lyne example D=0.3}
\end{figure}


\citet{Lyne2010} argue that the fast state changes seem to rule out free
precession as the origin of oscillatory behaviour observed in timing residuals.
One of the pulsars which shows some evidence for two state switching is PSR
B1828-11; this pulsar was cited as evidence for free precession by
\citet{Akgun2006}. \citet{Lyne2010} argue that the fluctuations from this pulsar
should be reinterpreted as two-state switch due to the observed fast state
changes. 

\citet{Jones2012} argues that such dismissal of precession is premature
since the modulation period of the switching has yet to be explained.  Instead,
the idea is raised that precession and magnetospheric switching are not
mutually exclusive.  Pulsars are most probably born in a randomly distributed
magnetospheric state, at least some may therefore exist under a delicate
balance between two states. Precession may be capable of periodically varying
the statistical probability of existing in one state or the other, sharp
changes would be caused by an `avalanche effect' as the particle energies reach
a threshold.  This provides the timescale for switching along with the ability
for the switching to be quasi-periodic since the precession only biases the
probability.

A similar idea considered by \citet{Cordes2013} interpreted two state switching
as evidence for a system in a state of stochastic resonance.  This occurs in
systems in which, under certain conditions, a weak periodic forcing function is
amplified by stochastic noise.  To explain this phenomenon in appendix
\ref{App: Stochastic} we present a toy model of stochastic resonance for a
particle in a well. The switching could therefore be the result of any periodic
modulation, such as precession, coupled to random fluctuations. This would quite
naturally explain the stability of states, the timescales over which the occur,
and the fact that it is observed in only some pulsars.

