Here we present a simple toy model of stochastic resonance. This is a
statistical phenomena occurring when a weak periodic forcing function is
amplified by noise (see \citet{Jung1991} for a full treatment).  For the
application to neutron stars, see \citet{Cordes2013}; here we simply aim to
describe the essential features of stochastic resonance (not its application to
NSs). 

We will consider a particle at a position $x$ which is subject to some
potential and acted upon by a forcing function $F(t)$. In general though, $x$
could be any state variable, thus stochastic resonance could be produced
in many systems.

First consider the static case of a particle in a potential $U(x)$  given by:
\begin{equation}
    U(x) = \frac{x^{4}}{4}-\frac{x^{2}}{2}. 
\end{equation}
This potential is characterised by two wells at $\pm1$, a maximum exists
between them at the origin. The particle in one of the wells sees a potential
barrier $\Delta U$ corresponding to the height of the maximum above its
position.

Assume the particle is acted upon by a random forcing function $F(t)$ which is
modelled as a Gaussian white noise with strength $D$. Depending on the
magnitude of $D$ with respect to the potential, the motion of the particle
admits two distinct cases:
\begin{enumerate}
\item $D \ll \Delta U \;\;$ in which case the particle remains inside whichever
    well it initially starts in and does not escape.
\item $D \gg \Delta U \;\;$ in this case the particle will not see the the
    individual wells only the larger one.
\end{enumerate}

The motion of the particle obeys the following equation of motion:
\begin{equation}
    \frac{dx}{dt} = -\frac{\partial V(x,t)}{\partial x} + F(t). 
\end{equation}
The motion of the particle has two components, the deterministic effect of the
potential and random fluctuations.

We now modify the potential to be acted on by a weak periodic function; this
introduced a third possible type of behaviour. Writing the time dependant
potential as
\begin{equation}
    V(x,t) = \frac{x^{4}}{4}-\frac{x^{2}}{2} + \epsilon x \cos(\omega_{0} t).
\end{equation}
Inserting this potential into the equations of motion:
\begin{equation}
    \frac{dx}{dt} =  x - x^{3} + F(t) + \epsilon \cos(\omega_{0} t).
\label{eqn:stochastic eom}
\end{equation}
Solving this numerically we fix $\epsilon=0.001$,
$\omega_{0}=\frac{2\pi}{10}$ and choose three values of $D$ which illustrate
typical behaviours of the solution
\begin{figure}[ht]
\centering
   \includegraphics[width=0.4\textwidth,trim=0mm -10mm 0mm 0mm]
   {{Stochastic_resonance}.png}

\caption{Three solutions to equation \eqref{eqn:stochastic eom} changing the
    random forcing functions strength $D$. The first and last panels show the
    deterministic solutions for the particle position: either the forcing
    function is weak compared to the potential, the particle remains in well in
    which it begins; or the forcing function is much stronger than the
    potential and so the particle freely moves about the two wells. The middle
    panel illustrates the special case of stochastic resonance whereby the
periodic fluctuations of the potential allow quasi-periodic variations in the
particles position between the two wells.}

\label{fig:stochastic resonance}
\end{figure}
The first and last runs replicate the behaviour expected  for a static well,
either the particle is confined to the well it starts in, or the random noise
is too strong and the individual wells are not observed. The middle case
displays strong stochastic resonance: the solution
displays a switching between bi-stable states but does not strictly follow the
period of the forcing function. The
important point here is that the forcing function may be weak, but provided it
is periodic or at least quasi-periodic the signal is amplified by the random
noise such that it may be visible in data sets where it would typically be
considered lost.

