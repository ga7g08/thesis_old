
An alternative motivation to study timing noise comes from the measurement of
anomalous braking indices. The pulsar braking index is defined by $n$ in
equation \ref{eqn: power law spindown}.  Rearranging this equation as in
\eqref{eqn: measured braking index} the braking index can be measured for
observed pulsars. Different types of braking exhibit different braking indices.
It is therefore a reasonable idea to measure the braking index and calculate
the type of braking. Pulsars spun down by an electromagnetic torque should
follow a braking index of $n=3$, while gravitational wave spindown has $n=5$.

Measuring these indices for the known pulsar population we do not find a consensus
on the type of braking. Values from from unity up to $10^{6}$ and even negative
braking indices have been measured. These are known as \emph{anomalous}
braking indices. 

Recent work by \citet{Biryukov2012} observed that
younger pulsars tend to have braking indices of the correct order of
magnitude. However, beyond~$\tau_{ch}\approx10^{5}$~years
the absolute value of the braking index rapidly grows,
reaching values as large as $10^{6}$ for the oldest pulsar. In addition an
almost equal number of pulsars have positive and negative values of the braking
index. The figure demonstrating this is plotted in
figure~\ref{fig: braking indices}.

\begin{figure}[ht]
\centering
	\includegraphics[width=0.5\textwidth,trim=0mm -10mm 0mm 0mm]
               {{Biryukov_2012_Figure_7}.png}
\caption{Pulsar population in the $n_{obs}-\tau_{ch}$ diagram image from
\citet{Biryukov2012}}
\label{fig: braking indices}
\end{figure}

\citet{Biryukov2012} proposed that the spindown $\dot{\nu}(t)$ may contain the
secular spin down $\dot{\nu}_{\textrm{sec}}(t)$ and a cyclic component
$\dot{\nu}_{\textrm{sec}}(t)\epsilon(t)\nu(t)$ oscillating the spindown about
a mean value. Taking a simple case where the cyclic term has the form $A
\cos\phi(t)$ where $A$ is the relative amplitude of the oscillations and
$\phi(t)$ is linear in $t$, the authors derive an equation for the observed braking
index

\begin{equation}
n_{obs}(t) =
\frac{n}{1+A\cos(\dot{\phi}t+\phi_0)}
+\frac{(n-1)(kt-c)}{(1+A\cos(\dot{\phi}t+\phi_{0}))^{2}}A\dot{\phi}\sin(\dot{\phi}t+\phi_{0}).
\label{eqn: nobs}
\end{equation}•

This observed braking index contains a constant positive term oscillating about
the true braking index and a term which grows linearly in time. The authors found 
that for $\tau_{ch}<10^{5}$ yrs the linear term is negligible and so we observe
approximately the real braking index $n$. At later times the linear term
drives the observed braking index to larger values while a sinusoidal term produces
positive and negative values. In figure \ref{fig: nobs} we plot the trajectory
of a single pulsar following equation \eqref{eqn: nobs}. The authors claim each
of the pulsars in \ref{fig: braking indices} is following a similar trajectory.

\begin{figure}[ht]
\centering
	\includegraphics[width=0.5\textwidth]
               {{Analytic_Monotonic_and_Cyclic}.png}
\caption{A sketch of the observed braking index according to
equation \eqref{eqn: nobs}, the values here are intended for a qualitative
overview rather than analysis. }
\label{fig: nobs}
\end{figure} 

This simplistic idea is able to explain some of the defining features of the
known pulsar population braking indices. This requires a mechanism to modulate
the spindown over long timescales. By fitting their model to data, they
estimate the timescale to be of the order $10^{3}-10^{4}$~years. At least one
physical model, precession, could produce variations on the required timescale.
However, this is significantly longer than the precession timescales invoked to
explain the fluctuations in timing residuals which were $1-10$~years.
