A mechanism which could quite naturally produce strictly periodic variations in the
observable features of a pulsar is \emph{free precession}. This occurs in any
non-spherical body for which the angular momentum is not aligned with a principle
axis of the moment of inertia. Such a circumstance could arise given the
chaotic birth of NSs. However, we must be clear that the timing noise induced by 
precession alone would be strictly deterministic; this is something which we do
not observe.
It is instructive however to consider the mechanics of precession since it will
be visited later on.

In the simplest case imagine an biaxial body, 
rotating about an axis $\Omega$. with a moment of inertia given by 
\begin{equation}
    I = \left[\begin{array}{ccc}
            I_{0} & 0 & 0 \\
            0 & I_{0} & 0 \\
            0 & 0 & I_{0}(1 + \epsilon)
            \end{array}\right],
\end{equation}
where $\epsilon \ll 1$ is the measure of oblateness or prolateness.  If the
body is free from torques, then in the rotating frame of the body, Euler's
equations of motion (see section \ref{sec: neutron star dynamics in the
rotating frame}) are given by
\begin{equation}
    I\dot{\bm{\Omega}} + \bm{\Omega} \times \left(I\bm{\Omega}\right)=0.
\end{equation}
This is a system of three coupled ODEs. Writing the components of the spin
vector as $\bm{\Omega} = [\Omega_{x}, \Omega_{y}, \Omega{z}]$, we have the
set of equations:
\begin{align}
\dot{\Omega}_x = -\Omega_y\Omega_z, &&
\dot{\Omega}_y = \Omega_x \Omega_z, &&
\dot{\Omega}_z = 0
\end{align}
We can find a solution by first realising that $\Omega_{z}=\mathrm{const}$.
We are then left with a set of
two coupled ODEs, solving these with appropriate intial conditions 
the solutions take the form
\begin{align}
    \Omega_{x} & = \Omega_{0}\sin(a_0)\sin\left(\Omega_{0}\cos(a_0)\epsilon t\right), \\
    \Omega_{y} & = \Omega_{0}\sin(a_0)\cos\left(\Omega_{0}\cos(a_0)\epsilon t\right),\\
    \Omega_{z} & = \Omega_0 \cos(a_0),
\end{align}
where $a_0$ is the angle between the spin-vector and the body frame $z$ axis and
$\Omega_0$ is the magnitude of the spin-vector. 

We observe that the spin axis of the body will trace out a cone about the $z$
principle axis of the moment of inertia with a period of
$\frac{1}{\Omega_{z}\epsilon}$.  The half-angle of the cone is set by the
initial conditions and will not evolve. This is the motion of free precession
and is illustrated in figure \ref{fig: precession}. 
\begin{figure}[htb]
\centering
\includegraphics[scale=0.2]{Precession.png}
\caption{Illustration of free precession for a simple biaxial body. The spin
    axis $\spin$ traces out a cone about the angular momentum vector $\mathbf{J}$.}
\label{fig: precession}
\end{figure}•

Neutron stars are assume to have a rigid crust, as such they may be non-axially
symmetric. Precession as a candidate to explain timing noise fluctuations was
first discussed by \citet{Ruderman1970}. He found that the free precession
period was, for reasonable values of the  ellipticity $\epsilon$, able to
explain periodic fluctuations in the Crab pulsar. Of the known Neutron star 
physics, one of the few mechanism that could operate over the time-scales observed
in timing residuals is free precession. 

The favoured interpretation for glitches poses a problem for sustained free
precession as an interpretation of timing noise. Theoretical models suggest the
interior of a neutron star is a superfluid; most of the moment of inertia is
contained in an array of vortices which are pinned to the crust.  Glitch events
correspond to the sudden unpinning of these vortices. It was shown by
\citet{Shaham1977} that for perfect pinning the free precession frequency and
geometry were modified resulting in no slowly oscillating long-lived modes. In
the case of imperfect pinning \citet{Sedrakian1999} found that long-lived modes
existed but where damped.

Despite the inconsistency with the superfluid pinning model for glitches,
evidence was presented by \citet{Stairs2000} of free precession in pulsar
B1828-11. They found the phase residuals and variations in the pulse profile
could be accounted for by precession.  This was followed up by detailed
modelling of the effects by \citet{Akgun2006}.

Including the spindown from an applied torque \citet{Cordes1993} noted that
free precession may be driven by fluctuations that counter the damping process;
in turn, the precession can drive torque fluctuations. The effect is most
noticeable in young pulsars. 

Work by \citet{Jones2001} compared a model of free precession against the handful
of proposed observations of free precession. Their model included the feedback
between the torque and precession and required only the crust to undergo precession.
In all but one case such a model was found to be consistent with the observations.


